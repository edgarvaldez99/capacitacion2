Patrones de Diseño de responsive design y páginas web de ejemplos:

1 - Patrón: "Mostly Fluid"
    Ejemplo: http://www.antarcticstation.org/

2 - Patrón: "Column Drop"
    Ejemplo: https://modernizr.com/

3 - Patrón: "Layout Shifter"
    Ejemplo: http://foodsense.is/

4 - Patrón: "Tyny Tweaks"
    Ejemplo: http://futurefriendlyweb.com/

5 - Patrón: "Off Canvas" (El Patrón más nuevo)


Consideracions de desarrollo:

1 - Para Smartphone trabajar en Portrait - Landscape (En vertical, en horizontal)

2 - Viewport - Página para ver la resolución: http://viewportsize.com/

3 - Unidades de medidas absolutas y relativas

4 - Densidad de pixel - Retina Display

5 - Los TAB, solo en Desktop existe el evento click, para los demás dispositivos existe el TAB
    No solo existen los TABs, con ellos existen los eventos a través de "Gestos"

6 - Estrategias: Graceful Degradation (Desktop first) o Progresive Enhancement (Mobile first) - (De dimensiones mas grandes a mas pequeñas, viceversa)

7 - Ultima consideración y la más importante: Haz que tu sitio sea ligero y cargue rápido


Paginas de utilidad>

BE Lazy.js: http://dinbror.dk/blazy/

Buscar librería externas en Javascript: https://cdnjs.com/

Levantar un servidor de estaticos con python : 
	1 - En la consola, dirijirse a la carpeta donde estan los archivos a servirse y luego
	2 - Escribir: python -m SimpleHTTPServer


Testear el sitio con remote debugging
	En android Ir a: 
		- Configuracion (Setting)
		- Sobre el dispositivo (About device)
		- Información de la Aplicación (Software info)
		- Hacer tap (click) en: Número de compilación (Build number)
		- Volver un menu arriba y allí ir a
		- Opciones de desarrollador (Developer options)
		- Activar USB debugging
		- Conectar el telefono a la computadora
		- Abrir una pestaña de Chrome
		- Abrir el Ispector de elementos
		- Y verificar nuestro sitio desde alli
